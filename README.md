# pokemon-api

## Getting started

To run only you need to exec next commands with docker ready:
~~~
./gradle build
docker-compose up -d 
~~~

If you need to pass the all test you can run: 
~~~
./gradle integrationTest
~~~

There are two test package:

- /test -> Unit Test
- /integration-test -> boot spring context with testcontainers for create a real database in a docker (mysql)

For CI/CD we have .gitlab-ci.yml


