package com.heytrade.pokemonapi.pokemon.domain.mother

import com.heytrade.pokemonapi.pokemon.domain.Pokemon
import com.heytrade.pokemonapi.pokemon.domain.PokemonType
import java.util.*

object PokemonMother {

    fun of(
        id: Long = 1,
        name: String = "pikachu",
        type: PokemonType = PokemonType.FLYING,
        weight: String = "",
        height: String = "",
        healthPoints: Int = 100,
        combatPoints: Int = 100,
        favorite: Boolean = false
    ): Pokemon {
        return Pokemon(
            id,
            name,
            type,
            weight,
            height,
            healthPoints,
            combatPoints,
            favorite
        )
    }
}