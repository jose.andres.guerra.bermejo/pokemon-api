package com.heytrade.pokemonapi.pokemon.application

import com.heytrade.pokemonapi.pokemon.application.usecases.FindByTypePokemonQry
import com.heytrade.pokemonapi.pokemon.domain.PokemonRepository
import com.heytrade.pokemonapi.pokemon.domain.PokemonType
import com.heytrade.pokemonapi.pokemon.domain.mother.PokemonMother
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

class FindByTypePokemonQryTest {


    @Test
    fun `when send a empty filter get all pokemon`(){
        val repository = mockk<PokemonRepository>()
        every { repository.findByType(PokemonType.ELECTRIC) } returns listOf(PIKACHU, RAICHU)

        val sut = FindByTypePokemonQry("ELECTRIC", repository).run()

        assertEquals(listOf(PIKACHU, RAICHU), sut)
    }

    companion object {
        private const val ID = 100L
        val PIKACHU = PokemonMother.of()
        val RAICHU = PokemonMother.of(id = ID, name = "Raichu")
    }

}