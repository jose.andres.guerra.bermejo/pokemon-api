package com.heytrade.pokemonapi.pokemon.application

import com.heytrade.pokemonapi.pokemon.application.usecases.FindAllPokemonQry
import com.heytrade.pokemonapi.pokemon.domain.PokemonRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

class FindAllPokemonQryTest {

    @Test
    fun `when run without favorite then exec repository findAll`(){
        val repository = mockk<PokemonRepository>()
        every { repository.findAll() } returns listOf()

        FindAllPokemonQry(repository, null).run()

        verify { repository.findAll() }
    }

    @Test
    fun `when run within favorite filter then exec findByFavorite`(){
        val repository = mockk<PokemonRepository>()
        every { repository.findByFavorite(true) } returns listOf()

        FindAllPokemonQry(repository, true).run()

        verify { repository.findByFavorite(true) }
    }
}