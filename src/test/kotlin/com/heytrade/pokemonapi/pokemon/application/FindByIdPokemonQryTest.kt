package com.heytrade.pokemonapi.pokemon.application

import com.heytrade.pokemonapi.pokemon.application.usecases.FindByIdPokemonQry
import com.heytrade.pokemonapi.pokemon.domain.PokemonRepository
import com.heytrade.pokemonapi.pokemon.domain.mother.PokemonMother
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

class FindByIdPokemonQryTest {

    @Test
    fun `when send ID then exec repository`(){
        val repository = mockk<PokemonRepository>()
        every { repository.findById(ID) } returns Optional.of(RAICHU)

        val sut = FindByIdPokemonQry(repository, ID).run().get()

        assertEquals(RAICHU, sut)
    }

    companion object {
        private val ID = 100L
        val RAICHU = PokemonMother.of(id = ID, name = "Raichu")
    }
}