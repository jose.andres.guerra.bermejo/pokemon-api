package com.heytrade.pokemonapi.pokemon.application

import com.heytrade.pokemonapi.pokemon.application.usecases.SearchByNamePokemonQry
import com.heytrade.pokemonapi.pokemon.domain.PokemonRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test

class SearchByNamePokemonQryTest {

    @Test
    fun `when run usecase then exec repository with given arguments`(){
        val repository = mockk<PokemonRepository>()
        every { repository.findByName(NAME) } returns listOf()

        SearchByNamePokemonQry(repository, NAME).run()

        verify { repository.findByName(NAME) }
    }

    companion object {
        const val NAME = "Pikachu"
    }
}