package com.heytrade.pokemonapi.pokemon.application

import com.heytrade.pokemonapi.pokemon.application.usecases.PatchFavoritePokemonCmd
import com.heytrade.pokemonapi.pokemon.domain.PokemonRepository
import com.heytrade.pokemonapi.pokemon.domain.mother.PokemonMother
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.util.*

class PatchFavoritePokemonCmdTest {

    @Test
    fun `when try save favorite pokemon then use repository operations`(){
        val repository = mockk<PokemonRepository>()
        val pokemonFilter = slot<Long>()
        every { repository.findById(capture(pokemonFilter)) } returns Optional.of(POKEMON)
        every { repository.save(POKEMON.copy(favorite = true)) } returns POKEMON.copy(favorite = true)

        PatchFavoritePokemonCmd(repository, ID, true).run()

        verify { repository.save(POKEMON.copy(favorite = true)) }
        assertEquals(ID, pokemonFilter.captured)
    }

    companion object {
        private val ID = 100L
        val POKEMON = PokemonMother.of(id = ID)
    }
}