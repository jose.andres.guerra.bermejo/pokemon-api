-- liquibase formatted sql

-- changeset liquibase:2
INSERT INTO pokemon(name, type, weight, height, health_points, combat_points, favorite) VALUES('pikachu','ELECTRIC','12-35kg','80-120cm',1000,2000, false);
INSERT INTO pokemon(name, type, weight, height, health_points, combat_points, favorite) VALUES('raichu','ELECTRIC','130-135kg','100-400cm',1000,2000, false);
INSERT INTO pokemon(name, type, weight, height, health_points, combat_points, favorite) VALUES('bulbasur','GRASS','120-350kg','802-900cm',1000,2000, false);
INSERT INTO pokemon(name, type, weight, height, health_points, combat_points, favorite) VALUES('ivisaur','GRASS','10-15kg','40-40cm',1000,2000, false);
INSERT INTO pokemon(name, type, weight, height, health_points, combat_points, favorite) VALUES('venusaur','GRASS','10-89kg','5-150cm',1000,2000, false);
INSERT INTO pokemon(name, type, weight, height, health_points, combat_points, favorite) VALUES('charmander','FIRE','10-20kg','60-170cm',1000,2000, false);
INSERT INTO pokemon(name, type, weight, height, health_points, combat_points, favorite) VALUES('charmeleon','FIRE','34-35kg','90-220cm',1000,2000, false);
INSERT INTO pokemon(name, type, weight, height, health_points, combat_points, favorite) VALUES('charizard','FIRE','100-600kg','20-400cm',1000,2000, false);
INSERT INTO pokemon(name, type, weight, height, health_points, combat_points, favorite) VALUES('squirtle','WATER','78-90kg','20-60cm',1000,2000, false);
INSERT INTO pokemon(name, type, weight, height, health_points, combat_points, favorite) VALUES('wartortle','WATER','100-235kg','50-80cm',1000,2000, false);
INSERT INTO pokemon(name, type, weight, height, health_points, combat_points, favorite) VALUES('blastoise','WATER','500-1000kg','100-520cm',1000,2000, false);