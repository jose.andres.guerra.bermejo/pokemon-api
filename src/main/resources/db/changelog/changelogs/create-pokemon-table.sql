-- liquibase formatted sql

-- changeset liquibase:1
CREATE TABLE pokemon (id INTEGER NOT NULL AUTO_INCREMENT, name VARCHAR(256), type VARCHAR(256), weight VARCHAR(256), height VARCHAR(256), health_points INTEGER, combat_points INTEGER, favorite BOOL, PRIMARY KEY (id));