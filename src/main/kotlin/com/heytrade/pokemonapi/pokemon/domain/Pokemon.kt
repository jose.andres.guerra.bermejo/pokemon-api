package com.heytrade.pokemonapi.pokemon.domain

import jakarta.persistence.*


@Entity
@Table
data class Pokemon(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long,
    val name: String,
    @Enumerated(EnumType.STRING)
    val type: PokemonType,
    val weight: String,
    val height: String,
    val healthPoints: Int,
    val combatPoints: Int,
    val favorite: Boolean
)