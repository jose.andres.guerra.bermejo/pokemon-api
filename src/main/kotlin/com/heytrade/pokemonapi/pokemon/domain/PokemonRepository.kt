package com.heytrade.pokemonapi.pokemon.domain

import java.util.Optional

interface PokemonRepository {
    fun save(pokemon: Pokemon): Pokemon
    fun findById(id: Long): Optional<Pokemon>
    fun findByName(name: String): List<Pokemon>
    fun findByType(type: PokemonType): List<Pokemon>
    fun findByFavorite(favorite: Boolean): List<Pokemon>
    fun findAll(): List<Pokemon>

}
