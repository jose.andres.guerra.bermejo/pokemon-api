package com.heytrade.pokemonapi.pokemon.domain

enum class PokemonType {
FIRE, FLYING, WATER, GRASS, POISON, BUG, ELECTRIC
}
