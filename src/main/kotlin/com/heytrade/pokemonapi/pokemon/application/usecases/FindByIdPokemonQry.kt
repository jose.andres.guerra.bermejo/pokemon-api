package com.heytrade.pokemonapi.pokemon.application.usecases

import com.heytrade.pokemonapi.pokemon.domain.Pokemon
import com.heytrade.pokemonapi.pokemon.domain.PokemonRepository
import java.util.*

class FindByIdPokemonQry(private val repository: PokemonRepository, private val id: Long) {
    fun run(): Optional<Pokemon> {
        return repository.findById(id)
    }
}