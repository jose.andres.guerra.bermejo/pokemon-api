package com.heytrade.pokemonapi.pokemon.application.usecases

import com.heytrade.pokemonapi.pokemon.domain.Pokemon
import com.heytrade.pokemonapi.pokemon.domain.PokemonRepository

class FindAllPokemonQry(private val repository: PokemonRepository, private val favorite: Boolean?) {
    fun run(): List<Pokemon> {
        return if(favorite != null) repository.findByFavorite(favorite)
        else repository.findAll()
    }
}