package com.heytrade.pokemonapi.pokemon.application.usecases

import com.heytrade.pokemonapi.pokemon.domain.Pokemon
import com.heytrade.pokemonapi.pokemon.domain.PokemonRepository
import com.heytrade.pokemonapi.pokemon.domain.PokemonType

class FindByTypePokemonQry(private val type: String, private val repository: PokemonRepository) {

    fun run(): List<Pokemon> {
        return repository.findByType(PokemonType.valueOf(type))

    }
}