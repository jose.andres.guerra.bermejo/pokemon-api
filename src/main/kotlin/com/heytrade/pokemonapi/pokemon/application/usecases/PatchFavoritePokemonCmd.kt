package com.heytrade.pokemonapi.pokemon.application.usecases

import com.heytrade.pokemonapi.pokemon.domain.Pokemon
import com.heytrade.pokemonapi.pokemon.domain.PokemonRepository

class PatchFavoritePokemonCmd(private val repository: PokemonRepository, private val pokemonId: Long, private val favorite: Boolean) {

    fun run(): Pokemon {
        val pokemon = repository.findById(pokemonId).get()
        return repository.save(pokemon.copy(favorite = favorite))
    }
}