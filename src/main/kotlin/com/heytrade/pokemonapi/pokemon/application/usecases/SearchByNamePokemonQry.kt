package com.heytrade.pokemonapi.pokemon.application.usecases

import com.heytrade.pokemonapi.pokemon.domain.Pokemon
import com.heytrade.pokemonapi.pokemon.domain.PokemonRepository

class SearchByNamePokemonQry(private val repository: PokemonRepository, private val name: String) {
    fun run(): List<Pokemon>{
        return repository.findByName(name)
    }
}