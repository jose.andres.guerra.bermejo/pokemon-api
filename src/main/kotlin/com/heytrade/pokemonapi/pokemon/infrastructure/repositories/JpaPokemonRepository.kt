package com.heytrade.pokemonapi.pokemon.infrastructure.repositories

import com.heytrade.pokemonapi.pokemon.domain.Pokemon
import com.heytrade.pokemonapi.pokemon.domain.PokemonRepository
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.UUID

@Repository
interface JpaPokemonRepository: PokemonRepository, JpaRepository<Pokemon, UUID>