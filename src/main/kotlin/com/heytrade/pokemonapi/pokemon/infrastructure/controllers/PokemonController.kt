package com.heytrade.pokemonapi.pokemon.infrastructure.controllers

import com.heytrade.pokemonapi.pokemon.application.usecases.*
import com.heytrade.pokemonapi.pokemon.domain.PokemonRepository
import com.heytrade.pokemonapi.pokemon.infrastructure.mappers.PokemonMapper
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PatchMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController


@RestController
@RequestMapping("/pokemon")
class PokemonController(private val repository: PokemonRepository, private val pokemonMapper: PokemonMapper) {

    @GetMapping
    @ResponseBody
    fun findAll(@RequestParam favorite: Boolean?): ResponseEntity<List<PokemonDto>> {
        return ResponseEntity.ok(
            pokemonMapper.listToDto(
                FindAllPokemonQry(repository, favorite).run()
            )
        )
    }

    @GetMapping("/filter")
    @ResponseBody
    fun filterByType(@RequestParam type: String):ResponseEntity<List<PokemonDto>> {
        return ResponseEntity.ok(
            pokemonMapper.listToDto(FindByTypePokemonQry(type, repository).run())
        )
    }

    @GetMapping("/search")
    @ResponseBody
    fun searchByName(@RequestParam name: String): ResponseEntity<List<PokemonDto>> {
        return ResponseEntity.ok(
            pokemonMapper.listToDto(SearchByNamePokemonQry(repository, name).run())
        )
    }


    @GetMapping("/{id}")
    @ResponseBody
    fun findById(@PathVariable id: Long): ResponseEntity<PokemonDto>{
        return FindByIdPokemonQry(repository, id).run()
            .map {
                ResponseEntity.ok(pokemonMapper.toDto(it))
        }.orElse( ResponseEntity.notFound().build())
    }

    @ResponseStatus(HttpStatus.ACCEPTED)
    @PatchMapping("/{id}")
    fun patch(@PathVariable id: Long, @RequestBody body: PokemonPathFilter): ResponseEntity<PokemonDto>{
        return ResponseEntity.ok(pokemonMapper.toDto(PatchFavoritePokemonCmd(repository, id, body.favorite).run()))
    }
}