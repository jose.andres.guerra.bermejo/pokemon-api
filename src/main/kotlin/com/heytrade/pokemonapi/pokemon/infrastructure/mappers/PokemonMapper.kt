package com.heytrade.pokemonapi.pokemon.infrastructure.mappers

import com.heytrade.pokemonapi.pokemon.domain.Pokemon
import com.heytrade.pokemonapi.pokemon.infrastructure.controllers.PokemonDto
import org.springframework.stereotype.Service

@Service
class PokemonMapper {

    fun listToDto(pokemon: List<Pokemon>): List<PokemonDto> {
        return pokemon.map { toDto(it) }
    }

    fun toDto(pokemon: Pokemon): PokemonDto {
        return PokemonDto(
            pokemon.id,
            pokemon.name,
            pokemon.type,
            pokemon.weight,
            pokemon.height,
            pokemon.healthPoints,
            pokemon.combatPoints,
            pokemon.favorite
        )
    }

    fun toDao(pokemonDto: PokemonDto): Pokemon {
        return Pokemon(
            pokemonDto.id,
            pokemonDto.name,
            pokemonDto.type,
            pokemonDto.weight,
            pokemonDto.height,
            pokemonDto.healthPoints,
            pokemonDto.combatPoints,
            pokemonDto.favorite
        )
    }
}