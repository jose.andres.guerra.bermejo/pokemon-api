package com.heytrade.pokemonapi.pokemon.infrastructure.controllers

import com.heytrade.pokemonapi.pokemon.domain.PokemonType

data class PokemonDto(
    val id: Long,
    val name: String,
    val type: PokemonType,
    val weight: String,
    val height: String,
    val healthPoints: Int,
    val combatPoints: Int,
    val favorite: Boolean
)
