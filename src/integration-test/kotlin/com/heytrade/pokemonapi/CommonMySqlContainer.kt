package com.heytrade.pokemonapi

import org.testcontainers.containers.MySQLContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.junit.jupiter.Testcontainers
import org.testcontainers.utility.DockerImageName

@Testcontainers
abstract class CommonMySqlContainer {

    companion object {
        @Container
        private val mysqlContainer = MySQLContainer(DockerImageName.parse("mysql:8.0.31")).apply {
            withDatabaseName("pokemon_db")
            withUsername("test")
            withPassword("test")
            portBindings.add("3306:3306")
        }
    }
}