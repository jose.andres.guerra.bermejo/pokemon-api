package com.heytrade.pokemonapi.pokemon.infrastructure.controllers

import com.heytrade.pokemonapi.CommonMySqlContainer
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch
import org.springframework.test.web.servlet.result.MockMvcResultMatchers

@SpringBootTest
@ActiveProfiles("test")
@AutoConfigureMockMvc
class PokemonControllerIT: CommonMySqlContainer() {

    @Autowired
    private lateinit var mockMvc: MockMvc

    //TODO validate return values in all tests
    @Test
    fun `when request GET to root path then get all pokemon`(){
        mockMvc.perform(get("/pokemon")).andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun `when request search then get a pokemon`(){
        mockMvc.perform(get("/pokemon/search?name=pikachu")).andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun `when request filter type then get several pokemon`(){
        mockMvc.perform(get("/pokemon/filter?type=ELECTRIC")).andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun `when filter by ID then get a pokemon`(){
        mockMvc.perform(get("/pokemon/1")).andExpect(MockMvcResultMatchers.status().isOk)
    }

    @Test
    fun `when request Path to save a favorite then get that pokemon`(){
        mockMvc.perform(patch("/pokemon/1").contentType(MediaType.APPLICATION_JSON)
            .content("{\"favorite\": true}")).andExpect(MockMvcResultMatchers.status().isOk)
    }
}