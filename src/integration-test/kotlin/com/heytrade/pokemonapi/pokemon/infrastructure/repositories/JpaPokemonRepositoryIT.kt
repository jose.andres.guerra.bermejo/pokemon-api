package com.heytrade.pokemonapi.pokemon.infrastructure.repositories

import com.heytrade.pokemonapi.CommonMySqlContainer
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.ActiveProfiles

@DataJpaTest
@ActiveProfiles("test")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class JpaPokemonRepositoryIT: CommonMySqlContainer() {

    @Autowired
    private lateinit var repository: JpaPokemonRepository

    @Test
    fun `findAll - when find all pokemon then get a measurable list`(){
        val sut = repository.findAll()

        assertTrue(sut.isNotEmpty())
        assertEquals(11,  sut.size)
    }

    @Test
    fun `findById - when find by ID then get a Pokemon`(){
        val pokemon = repository.findAll().first()

        val sut = repository.findById(pokemon.id).get()

        assertEquals(pokemon, sut)
    }
}